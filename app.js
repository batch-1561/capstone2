//[SECTIONS] Dependencies and Modules
	const express = require('express');
	const mongoose = require('mongoose');
	const dotenv = require('dotenv');
	/*const courseRoutes = require('./routes/courses');
	const userRoutes = require('./routes/users');*/

//[SECTIONS] Environment Variables Setup
	dotenv.config();
	const port = process.env.PORT;
	const credentials = process.env.MONGO_URL;

//[SECTIONS] Server Setup
	const app = express();
	/*app.use(express.json());*/

//[SECTIONS] Database Connect
	mongoose.connect(credentials);
	const db = mongoose.connection;
	db.once('open',() => {
		console.log(`Connected to Atlas!`)
	});

//[SECTIONS] Server Routes
	/*app.use('/courses', courseRoutes);
	app.use('/users', userRoutes);*/

//[SECTIONS] Server Responses
	/*app.get('/', (res, req) => {
		res.send(`Project Deployed Successfully`);
	});*/
	app.listen(port, () => {
		console.log(`API is now online on port ${port}`);
	});